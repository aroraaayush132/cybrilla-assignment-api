import { Injectable } from '@nestjs/common';
import { CreateShortnerDto } from '../dto/create-shortner.dto';
import { ShowLinkDto } from '../dto/show-link.dto';
import * as ShortId from 'shortid';
import { UrlHasher } from '../helpers/url-hasher.helper';
import { LinkEntity } from '../db/entities/link.entity';

@Injectable()
export class UrlShortnerService {
  async create(dto: CreateShortnerDto): Promise<ShowLinkDto> {
    const hasher = new UrlHasher(dto.longUrl);
    const existing = await LinkEntity.findOne({ urlHash: hasher.hash });

    if (existing) {
      const ret = new ShowLinkDto();
      ret.longUrl = existing.url;
      ret.shortLink = `http://localhost:3000/${existing.code}`;

      return ret;
    }

    const shortCode = ShortId.generate()
      .replace(/[^\w\d]/, '')
      .substring(0, 6);

    const link = LinkEntity.create({
      url: dto.longUrl,
      urlHash: hasher.hash,
      code: shortCode,
    });

    const created = await LinkEntity.save(link);

    const obj = new ShowLinkDto();
    obj.longUrl = link.url;
    obj.shortLink = `http://localhost:3000/${link.code}`;

    return obj;
  }
}
