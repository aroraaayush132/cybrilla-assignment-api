import { Injectable } from '@nestjs/common';
import UserEntity from '../db/entities/user.entity';

@Injectable()
export class GoogleService {
  async googleLogin(req) {
    const user: UserEntity = await UserEntity.getUserByEmail(req.user.email);

    if (!user) {
      return 'User does not exists';
    }
    if (!req.user) {
      return 'No user from google';
    }

    return {
      message: 'User information from google',
      user: req.user,
    };
  }
}
