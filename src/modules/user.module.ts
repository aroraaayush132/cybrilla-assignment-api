import { Module } from '@nestjs/common';
import { UserController } from '../controllers/user.controller';
import { UserService } from '../services/user.service';
import AuthService from '../services/auth.service';
import { GoogleStrategy } from '../guards/google.strategy';

@Module({
  imports: [],
  controllers: [UserController],
  providers: [UserService, AuthService, GoogleStrategy],
})
export class UserModule {}
