import { Body, Controller, Header, Post, Req, UseGuards } from '@nestjs/common';
import { UrlShortnerService } from '../services/url-shortner.service';
import { Request } from 'express';
import { CreateShortnerDto } from '../dto/create-shortner.dto';
import { ShowLinkDto } from '../dto/show-link.dto';
import AuthenticationGuard from '../guards/authentication.guard';
import RolesGuard from '../guards/roles.guard';
import EAccess from '../enums/access.enum';

@Controller('url-shortner')
export class UrlShortnerController {
  constructor(private readonly urlShortnerService: UrlShortnerService) {}

  @Post('short_link')
  @Header('Content-Type', 'application/json')
  @UseGuards(AuthenticationGuard, new RolesGuard([EAccess.ADMIN, EAccess.USER]))
  async create(
    @Req() request: Request,
    @Body() dto: CreateShortnerDto,
  ): Promise<ShowLinkDto> {
    return this.urlShortnerService.create(dto);
  }
}
