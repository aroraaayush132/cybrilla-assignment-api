import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { UserModule } from './modules/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { GoogleController } from './controllers/google.controller';
import { UrlShortnerController } from './controllers/url-shortner.controller';
import { UserController } from './controllers/user.controller';
import { GoogleService } from './services/google.service';
import { UserService } from './services/user.service';
import { UrlShortnerService } from './services/url-shortner.service';
import AuthService from './services/auth.service';

@Module({
  imports: [UserModule, TypeOrmModule.forRoot(typeOrmConfig)],
  controllers: [UrlShortnerController, GoogleController, UserController],
  providers: [
    AppService,
    AuthService,
    GoogleService,
    UrlShortnerService,
    UserService,
  ],
})
export class AppModule {}
