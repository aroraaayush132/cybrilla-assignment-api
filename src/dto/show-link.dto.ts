export class ShowLinkDto {
  longUrl: string;
  shortLink: string;
}
