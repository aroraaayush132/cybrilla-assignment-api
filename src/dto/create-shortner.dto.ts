import { IsUrl, Length } from 'class-validator';

export class CreateShortnerDto {
  @IsUrl()
  @Length(1, 2000)
  longUrl: string;
}
